<?php

// Connexion simple à la base de données via PDO !
$db = new PDO('mysql:host=localhost;dbname=****;charset=utf8', '****', '****', [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
]);

