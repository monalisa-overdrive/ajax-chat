<?php

require_once('connect.php');

// On doit analyser la demande faite via l'URL (GET) afin de déterminer si on souhaite récupérer les messages ou en écrire un
$task = "list";

if(array_key_exists("task", $_GET)){
  $task = $_GET['task'];
}

if($task == "write"){
  postMessage();
} else {
  getMessages();
}

// Si on veut récupérer, il faut envoyer du JSON
function getMessages(){
  global $db;
  // suppression des messages vieux de plus d'une minute
  $delete = $db->query("DELETE FROM livechat_messages WHERE created_at < SUBTIME(NOW(), '00:01:00')") ;
  // 1. On requête la base de données pour sortir les messages
  $resultats = $db->query("SELECT * FROM livechat_messages ORDER BY created_at DESC");
  // 2. On traite les résultats
  $messages = $resultats->fetchAll();
  // 3. On affiche les données sous forme de JSON
  echo json_encode($messages);
}

// Si on veut écrire au contraire, il faut analyser les paramètres envoyés en POST et les sauver dans la base de données
function postMessage(){
  global $db;

  // 1. Analyser les paramètres passés en POST (author, content)
  if(!array_key_exists('author', $_POST) || !array_key_exists('content', $_POST)){

    echo json_encode(["status" => "error", "message" => "One field or many have not been sent"]);
    return;

  }

  $author = htmlspecialchars($_POST['author']);
  $content = htmlspecialchars($_POST['content']);

  // 2. Créer une requête qui permettra d'insérer ces données
  $query = $db->prepare('INSERT INTO livechat_messages SET author = :author, content = :content, created_at = NOW()');

  $query->execute([
    "author" => $author,
    "content" => $content
  ]);

  // 3. Donner un statut de succes ou d'erreur au format JSON
  echo json_encode(["status" => "success"]);
}
